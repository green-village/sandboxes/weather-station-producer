# Weather station

https://www.weatherlink.com/

Vantage Pro2 Console:
https://www.davisinstruments.com/solution/vantage-pro2/

WeatherLinkIP Data Logger:
https://www.davisinstruments.com/product/weatherlinkip-for-vantage-stations/

```
mkvirtualenv weather
pip install -r requirements.txt

workon weather
```

## WeatherLink API

https://www.weatherlink.com/static/docs/APIdocumentation.pdf

```
export USER=
export PASS=
export TOKEN=

curl 'https://api.weatherlink.com/v1/NoaaExt.json?user=$USER&pass=$PASS&apiToken=TOKEN' | jq .
curl 'https://api.weatherlink.com/v1/StationStatus.json?user=$USER&pass=$PASS&apiToken=TOKEN' | jq .
```

## rc.local

Run the python script at startup.

`/etc/rc.local`

```
/home/pi/berryconda3/bin/python /home/pi/weather/weatherlink-rest.py 1>/dev/null 2>&1 &
/home/pi/berryconda3/bin/python /home/pi/weather/weatherlink-rest.py 1>/home/pi/weather/weatherlink-rest.log 2>&1 &
```
