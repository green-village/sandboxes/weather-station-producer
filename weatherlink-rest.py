"""Connector between the weather station and The Green Village data platform.

https://www.weatherlink.com/

WeatherLink API
https://www.weatherlink.com/static/docs/APIdocumentation.pdf

Vantage Pro2 Console
https://www.davisinstruments.com/solution/vantage-pro2/

WeatherLinkIP Data Logger
https://www.davisinstruments.com/product/weatherlinkip-for-vantage-stations/
"""

import json
import requests
from requests.exceptions import RequestException
import logging
import pathlib
import time
from datetime import datetime
from configparser import ConfigParser


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__file__)

parent_dir = pathlib.Path(__file__).parent.resolve()

# Read config file
config = ConfigParser()
config.read(str(parent_dir / 'weather.cfg'))
weather_user = config.get('WeatherLink', 'user')
weather_pass = config.get('WeatherLink', 'pass')
weather_token = config.get('WeatherLink', 'token')
rest_url = config.get('RestProxy', 'url')
rest_username = config.get('RestProxy', 'username')
rest_password = config.get('RestProxy', 'password')

weather_url = f"https://api.weatherlink.com/v1/NoaaExt.json?user={weather_user}&pass={weather_pass}&apiToken={weather_token}"

# Read measurements every minute.
PERIOD = 60


# Quantities to read
measurements = [
    {"name": "dewpoint_c", "description": "Dew point", "unit": "°C", "type": "FLOAT"},
    {"name": "heat_index_c", "description": "Heat index", "unit": "°C", "type": "FLOAT"},
    {"name": "pressure_mb", "description": "Pressure", "unit": "mb", "type": "FLOAT"},
    {"name": "relative_humidity", "description": "Relative humidity", "unit": "%", "type": "FLOAT"},
    {"name": "temp_c", "description": "Temperature", "unit": "°C", "type": "FLOAT"},
    {"name": "wind_degrees", "description": "Wind direction", "unit": "degrees", "type": "FLOAT"},
    {"name": "wind_dir", "description": "Wind direction", "unit": "", "type": "STRING"},
    {"name": "wind_kt", "description": "Wind speed", "unit": "kt", "type": "FLOAT"},
    {"name": "windchill_c", "description": "Wind chill", "unit": "°C", "type": "FLOAT"},
]


last_timestamp = 0
status_code = 0
while True:

    try:
        r = requests.get(weather_url, timeout=5)
        logger.debug(r.text)
        logger.info(f"WeatherLink API response code: {r.status_code}")

    except RequestException as e:
        logger.error(e)
        continue

    msg_json = r.json()

    # Time conversion
    # "observation_time_rfc822": "Wed, 21 Aug 2019 23:13:40 +0200"
    observation_time_rfc822 = msg_json["observation_time_rfc822"]
    observation_time_ms = int(datetime.strptime(observation_time_rfc822, '%a, %d %b %Y %H:%M:%S %z').timestamp() * 1000)

    # Has this message been seen/uploaded already?
    if observation_time_ms == last_timestamp and status_code == 200:
        logger.warning(f"The measurement from {observation_time_rfc822} has been seen/uploaded already.")
        time.sleep(PERIOD)
        continue
    last_timestamp = observation_time_ms


    values = []
    for m in measurements:

        if m["type"] == "FLOAT":
            v = {"float": float(msg_json[m["name"]])}
        elif m["type"] == "STRING":
            v = {"string": msg_json[m["name"]]}

        value = {
            "name": m["name"],
            "description": m["description"],
            "unit": m["unit"],
            "type": m["type"],
            "value": v
        }
        values.append(value)

    value = {
        "metadata": {
            "project_id": "weather",
            "device_id": msg_json["davis_current_observation"]["DID"],
            "description": "",
            "type": {"string": "Vantage Pro2"},
            "manufacturer": {"string": "Davis Instruments"},
            "serial": {"null": None},
            "placement_timestamp": {"null": None},
            "location": {"string": msg_json["location"]},
            "latitude": {"float": float(msg_json["latitude"])},
            "longitude": {"float": float(msg_json["longitude"])},
            "altitude": {"null": None}
        },
        "data": {
            "project_id": "weather",
            "device_id": msg_json["davis_current_observation"]["DID"],
            "timestamp": observation_time_ms,
            "values": values
        }
    }

    data = {
        "value_schema_id": 1,
        "records": [
            {
                "value": value
            }
        ]
    }
    logger.debug(value)


    try:
        headers = {
            "Content-Type": "application/vnd.kafka.avro.v2+json",
            "Accept": "application/vnd.kafka.v2+json"
        }
        r = requests.post(rest_url, json=data, headers=headers,
                auth=(rest_username, rest_password), timeout=5)
        status_code = r.status_code
        logger.debug(r.text)
        logger.info(f"Kafka REST Proxy response code: {r.status_code}")

    except RequestException as e:
        logger.error(e)
        continue

    time.sleep(PERIOD)
